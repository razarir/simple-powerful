<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div id="app" class="container grid grid-cols-3 min-h-screen bg-blue-100 mx-auto mt-4 p-1 gap-1">
    <index></index>
</div>
<script src="{{asset('/js/app.js')}}"></script>
{{--<script >
    window.user=@json(new App\Http\Resources\UserResource(auth()->user()));

    window.token='{{auth()->user()->createToken('spa')->plainTextToken}}'
</script>--}}
</body>
</html>
