import {createStore} from "vuex";
import swal from "sweetalert";

export default  createStore({
    state() {
        return {
            posts: [
                // {
                //     "id": 230,
                //     "title": "Principal Paradigm Supervisor Principal Paradigm Supervisor Principal Paradigm Supervisor ",
                //     "description": "Reiciendis aut culpa debitis dolorem incidunt natus. Quam veniam fugit et soluta. Hic rerum culpa. Et perspiciatis possimus sed.\n" +
                //         " ",
                //     "updated_at": "2022-06-16T07:32:22.000000Z",
                //     "image": "http://narin/storage/post\\230\\image\\350\\flowers-garden-petunia.webp",
                //     "gallery": [],
                //     "tags": [],
                //     "media": [
                //         {
                //             "id": 350,
                //             "original_url": "http://narin/storage/post\\230\\image\\350\\flowers-garden-petunia.webp",
                //             "collection_name": "image"
                //         }
                //     ]
                // }
            ],
            token: '' ?? localStorage.getItem('token'),
            user: {} ?? localStorage.getItem('user')
        }
    },
    mutations: {
        addPost(state, post) {
            state.posts.unshift(post)
        },
        setToken(state, token) {
            localStorage.setItem('token', token)
            state.token = token;
        },
        setUser(state, user) {
            localStorage.setItem('user_id', user.id)
            localStorage.setItem('user_name', user.name)
            state.user = user;
        },
        error(state, error) {
            var title = '';
            var text = '';
            if (error.response) {
                if (error.response.data.errors) {
                    title = ' (' + error.response.status + ')'
                    _.forEach(error.response.data.errors, function (e) {
                        text += e[0] + '\n'
                    });
                } else {
                    title = error.response.data.message + ' (' + error.response.status + ')'
                }
            } else if (error.request) {
                title = error.request
            } else {
                title = error.message
            }

            swal({
                title: title,
                text: text,
                icon: "error",
            });
        },
        success(state, success) {
            var title = success.title ?? '';
            var text = success.text ?? '';


            swal({
                title: title,
                text: text,
                icon: "success",
            });
        },
    },
    actions: {
        register({commit}, data) {
            return new Promise((resolve, reject) => {
                axios.post('/api/register', data)
                    .then(resp => {
                        commit('setToken', resp.data.token)
                        commit('setUser', resp.data.user)
                        commit('success', {title: 'register successful.'})
                        resolve(resp)
                    })
                    .catch((error) => {
                        commit('error', error)
                        reject(error)
                    })
            })
        },
        login({commit}, data) {
            return new Promise((resolve, reject) => {
                axios.post('/api/login', data)
                    .then(resp => {
                        commit('setToken', resp.data.token)
                        commit('setUser', resp.data.user)
                        commit('success', {title: 'login successful.'})

                        resolve(resp)
                    })
                    .catch((error) => {
                        commit('error', error)
                        reject(error)
                    })
            })
        },
        logout({commit}) {
            return new Promise((resolve, reject) => {
                axios.post('/api/logout')
                    .then(resp => {
                        commit('setToken', '')
                        commit('setUser', {})
                        commit('success', {title: 'logout successful.'})
                        resolve(resp)
                    })
                    .catch((error) => {
                        commit('error', error)
                        reject(error)
                    })
            })
        },
        postStore({commit, state}, data) {
            var formData = new FormData();
            _.forEach(data, function (value, index) {
                if (_.isArray(value)) {
                    value.forEach(function (item) {
                        formData.append(index + "[]", item);
                    });
                } else {
                    if (!_.isEmpty(value) || value instanceof File) {
                        formData.append(index, value);
                    }
                }
            })


            return new Promise((resolve, reject) => {
                axios.post('/api/post', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        "Authorization": "Bearer " + state.token
                    }
                })
                    .then(resp => {
                        commit('addPost', resp.data.data)
                        resolve(resp)
                    })
                    .catch((error) => {
                        commit('error', error)
                        reject(error)
                    })
            })
        },
        postIndex({commit}) {


            return new Promise((resolve, reject) => {
                axios.get('/api/post')
                    .then(resp => {
                        resp.data.data.forEach(function (post) {
                            commit('addPost', post)
                        });
                        resolve(resp)
                    })
                    .catch((error) => {
                        commit('error', error)
                        reject(error)
                    })
            })
        }
    }
})

