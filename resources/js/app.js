import './bootstrap';

import {createApp} from 'vue'
import store from './store/index'
import index from './components/index'

const app = createApp({
    data() {
        return {
            message: 'Hello Vue!'
        }
    },
    components: {
        index
    },
    mounted() {
        this.$store.commit('setToken', localStorage.getItem('token'))
        if (localStorage.getItem('user_id')) {
            this.$store.commit('setUser', {
                id: Number(localStorage.getItem('user_id')),
                name: localStorage.getItem('user_name')
            })
        }
    },
})

app.use(store)
app.mount('#app')
