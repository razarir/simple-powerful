<?php

namespace Tests\App\Models;

use App\Models\Post;
use App\Models\User;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{

    public function testUser()
    {
        $email = 'ali@example1.com' . random_int(1, 9999);
        $user = User::create(['name' => 'ali1', 'email' => $email, 'password' => 'password']);
//        dd($user);
//        $user = User::factory(1)->create()->first();
//
//        dump($user);
//        $post=Post::factory(1)->state([
//            'user_id' => $user->id
//        ])->create()->first();
////        $post->user()->associate($user);
        $this->assertEquals($user->email, $email);
    }
}
