<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ExampleEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $post;

    public function __construct($post)
    {
        $this->post = $post;
    }


    public function broadcastOn(): Channel
    {
        return new Channel('test');
//        return new PrivateChannel('test');
//        return new PresenceChannel('test');
    }

    public function broadcastAs()
    {
        return 'zzz';
    }

    public function broadcastWith()
    {
        return [
            'type' => 'addPost',
            'post' => $this->post,
        ];
    }
}
