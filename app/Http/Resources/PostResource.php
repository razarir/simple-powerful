<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Post */
class PostResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'updated_at' => $this->updated_at,
            'image' =>
                $this->when($this->relationLoaded('media'), $this->getFirstMediaUrl('image')),
            'gallery' =>
                $this->when($this->relationLoaded('media'), $this->getMedia('gallery')->pluck('original_url')),
            'tags' =>
                TagResource::collection($this->whenLoaded('tags')),
            'media' =>
                MediaResource::collection($this->whenLoaded('media')),
            'user' => new UserResource($this->whenLoaded('user')),
        ];
    }
}
