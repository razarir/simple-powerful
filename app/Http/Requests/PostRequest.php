<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PostRequest extends FormRequest
{
    public function rules(): array
    {
        $list = [
            'title' => 'required|min:3|max:255|',
            'description' => 'required|min:3|max:4000|',
            'gallery' => 'array|max:10',
            'gallery.*' => 'image',
            'tags' => 'array|min:1'
        ];
        if ($this->routeIs('post.store')) {
            return [
                ...$list,
                'image' => 'image',
//                'image' => 'required|image',
            ];
        } elseif ($this->routeIs('post.update')) {
            return [
                ...$list,
                'image' => 'image',
            ];
        }
    }

    public function authorize(): bool
    {
        return true;
    }

}
