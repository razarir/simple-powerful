<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(AuthRequest $request)
    {
        $request->merge(['password' => bcrypt($request->password)]);
        $user = User::create($request->only('password', 'email', 'name'));
        $user->assignRole('writer');
        $token = $user->createToken('spa');
        return ['token' => $token->plainTextToken, 'user' => new UserResource($user)];
    }

    public function login(AuthRequest $request)
    {
        if (auth()->attempt($request->validated())) {
            $user = auth()->user();
            $user->tokens()->whereName('spa')->delete();
            $token = $user->createToken('spa');
            return ['token' => $token->plainTextToken, 'user' => new UserResource($user)];
        } else {
            abort(401, 'Invalid Login Data');
        }
    }

    public function logout()
    {
        auth()->logout();
        abort(200, 'logout successful');

    }
}
