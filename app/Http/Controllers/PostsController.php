<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Post::class, 'post',[
            'except' => [ 'index', 'show' ],
        ]);
    }

    public function index()
    {
        return PostResource::collection(Post::with('user', 'media', 'tags')->latest()->paginate(10));
    }


    public function store(PostRequest $request)
    {
        $post = Post::create([...$request->validated(), 'user_id' => auth()->id()]);
        if ($tags = $request->tags) {
            $post->attachTags($tags);
        }
        if ($image = $request->image) {
            $post->addMedia($image)->toMediaCollection('image');
        }
        if ($request->gallery && count($gallery = $request->gallery)) {
            foreach ($gallery as $item) {
                $post->addMedia($item)->toMediaCollection('gallery');
            }
        }
        $postResource = new PostResource($post->load('media', 'tags','user'));
        broadcast(new \App\Events\ExampleEvent($postResource))
            ->toOthers();
        return $postResource;
    }

    public function show(Post $post)
    {
        return new PostResource($post->load('user', 'media', 'tags'));
    }


    public function update(PostRequest $request, Post $post)
    {
        $post->update([
            'title' => $request->title,
            'description' => $request->description,
        ]);
        if ($tags = $request->tags) {
            $post->syncTags($tags);
        }
        if ($image = $request->image) {
            $post->clearMediaCollection('image');
            $post->addMedia($image)->toMediaCollection('image');
        }
        if (is_array($request->gallery) && count($request->gallery)) {
            $post->clearMediaCollection('gallery');
            foreach ($request->gallery as $item) {
                $post->addMedia($item)->toMediaCollection('gallery');
            }
        }
        return new PostResource($post->load('media', 'tags'));
    }

    public function destroy(Post $post)
    {
        $post->delete();
    }
}
